use std::collections::HashMap;

// renders egui with bgfx as the backend
use bgfx::*;
use bgfx_rs::bgfx;
use egui::epaint::*;
use egui::*;
use egui_winit::winit;
use std::path::PathBuf;
//pub use winit;
//use winit::event;

use crate::utils;

fn load_shader_file(name: &str) -> std::io::Result<Vec<u8>> {
    let mut path = PathBuf::with_capacity(512);
    path.push("c:/src/rust/equinox/runtime/shaders");
    match bgfx::get_renderer_type() {
        RendererType::Direct3D11 => path.push("dx11"),
        RendererType::OpenGL => path.push("glsl"),
        RendererType::Metal => path.push("metal"),
        RendererType::OpenGLES => path.push("essl"),
        RendererType::Vulkan => path.push("spirv"),
        e => panic!("Unsupported render type {:#?}", e),
    }
    path.push(format!("{}.bin", name));
    println!("trying to load shader file: {}", path.as_path().display());
    let mut data = std::fs::read(path)?;
    data.push(0);
    Ok(data)
}

fn load_shader_program(vs: &str, ps: &str) -> std::io::Result<Program> {
    let vs_data = load_shader_file(vs)?;
    let ps_data = load_shader_file(ps)?;
    let vs_data = bgfx::Memory::copy(&vs_data);
    let ps_data = bgfx::Memory::copy(&ps_data);
    let vs_shader = bgfx::create_shader(&vs_data);
    let ps_shader = bgfx::create_shader(&ps_data);
    Ok(bgfx::create_program(&vs_shader, &ps_shader, false))
}

pub struct Painter {
    max_texture_size: u32,
    state: u64,
    program: bgfx::Program,
    vertex_layout: bgfx::VertexLayoutBuilder,
    vertex_buffer: bgfx::DynamicVertexBuffer,
    index_buffer: bgfx::DynamicIndexBuffer,
    textures: HashMap<egui::TextureId, bgfx::Texture>,
    textures_to_destroy: Vec<bgfx::Texture>,
    texture_uni: bgfx::Uniform,
}

impl Painter {
    pub fn new() -> Result<Painter, String> {
        let caps = bgfx::CapsLimits::new();
        // TODO waiting on bgfx-rs maintainer; texture size is 4096 on my machine lol
        let max_texture_size = caps.max_texture_size;
        println!("{}", max_texture_size);

        // TODO use include_bytes! macro
        let program = load_shader_program("vs_egui", "ps_egui").expect("failed to load shaders");

        let vertex_layout = bgfx::VertexLayoutBuilder::new();
        vertex_layout.begin(bgfx::RendererType::Noop);
        vertex_layout
            .add(
                bgfx::Attrib::Position,
                2,
                AttribType::Float,
                AddArgs::default(),
            )
            .add(
                bgfx::Attrib::TexCoord0,
                2,
                AttribType::Float,
                AddArgs::default(),
            )
            .add(
                bgfx::Attrib::Color0,
                4,
                AttribType::Uint8,
                AddArgs {
                    normalized: true,
                    as_int: false,
                },
            );
        vertex_layout.end();

        let ibuf_size = 5_000;
        let vbuf_size = 5_000;
        let vertex_buffer = bgfx::create_dynamic_vertex_buffer(
            vbuf_size,
            &vertex_layout,
            bgfx::BufferFlags::ALLOW_RESIZE.bits(),
        );

        let index_buffer = bgfx::create_dynamic_index_buffer(
            ibuf_size,
            (bgfx::BufferFlags::ALLOW_RESIZE | bgfx::BufferFlags::INDEX_32).bits(),
        );

        let state = 0u64
            | (StateWriteFlags::R | StateWriteFlags::G | StateWriteFlags::B | StateWriteFlags::A)
                .bits()
            | utils::bgfx_state_blend_func(
                // correct, confirmed in PIX and renderdoc, as well as checked outtput with the c++ version
                StateBlendFlags::ONE.bits(),           // srcColor blend
                StateBlendFlags::INV_SRC_ALPHA.bits(), // dstColor blend
                StateBlendFlags::INV_DST_ALPHA.bits(), // alphaSrc blend
                StateBlendFlags::ONE.bits(),           // dstColor blend
            );

        let texture_uni = Uniform::create("s_texColor", UniformType::Sampler, 1); // default is 1
        Ok(Painter {
            max_texture_size,
            program,
            vertex_layout,
            vertex_buffer,
            index_buffer,
            textures: Default::default(),
            textures_to_destroy: Vec::new(),
            texture_uni,
            state,
        })
    }

    fn paint_mesh(
        &mut self,
        pixels_per_point: f32,
        vertex_offset: u32,
        index_offset: u32,
        clip_rect: &emath::Rect,
        primitive: &epaint::Primitive,
    ) {
        let (width_in_pixels, height_in_pixels) =
            (bgfx::get_stats().width, bgfx::get_stats().height);

        //let first_index = index_offset;
        let clip_min_x = pixels_per_point * clip_rect.min.x;
        let clip_min_y = pixels_per_point * clip_rect.min.y;
        let clip_max_x = pixels_per_point * clip_rect.max.x;
        let clip_max_y = pixels_per_point * clip_rect.max.y;

        // Make sure clip rect can fit within a `u32`:
        let clip_min_x = clip_min_x.clamp(0.0, width_in_pixels as f32);
        let clip_min_y = clip_min_y.clamp(0.0, height_in_pixels as f32);
        let clip_max_x = clip_max_x.clamp(clip_min_x, width_in_pixels as f32);
        let clip_max_y = clip_max_y.clamp(clip_min_y, height_in_pixels as f32);

        let clip_min_x = clip_min_x.round() as u16;
        let clip_min_y = clip_min_y.round() as u16;
        let clip_max_x = clip_max_x.round() as u16;
        let clip_max_y = clip_max_y.round() as u16;
        match primitive {
            epaint::Primitive::Mesh(mesh) => {
                bgfx::set_scissor(
                    clip_min_x,
                    clip_min_y,
                    clip_max_x - clip_min_x,
                    clip_max_y - clip_min_y,
                );

                if let Some(texture) = self.get_texture(mesh.texture_id) {
                    // color and alpha blend funcs are set in Painter::new via bgfx state bits
                    bgfx::set_texture(
                        0,
                        &self.texture_uni,
                        &texture,
                        (SamplerFlags::UVW_CLAMP).bits(),
                    );
                }

                bgfx::set_dynamic_vertex_buffer(
                    0,
                    &self.vertex_buffer,
                    vertex_offset,
                    mesh.vertices.len() as _,
                );

                self.index_buffer
                    .set_dynamic_index_buffer(index_offset, mesh.indices.len() as u32);
                bgfx::set_state(self.state, std::u32::MAX);
                bgfx::submit(0, &self.program, SubmitArgs::default());
            }
            epaint::Primitive::Callback(pc) => unimplemented!(),
        }
    }

    pub fn paint_and_update_textures(
        &mut self,
        pixels_per_point: f32, // this comes from the egui Context
        clipped_meshes: &[egui::epaint::ClippedPrimitive],
        texture_delta: &egui::TexturesDelta,
    ) {
        for (id, image_delta) in &texture_delta.set {
            self.set_texture(*id, image_delta); // TODO update texture not  set
        }

        let mut vertex_offset = 0;
        let mut index_offset = 0;

        let vtx: Vec<Vertex> = clipped_meshes
            .iter()
            .map(|x| match &x.primitive {
                egui::epaint::Primitive::Mesh(mesh) => mesh.vertices.clone(),
                _ => unimplemented!(),
            })
            .collect::<Vec<Vec<Vertex>>>()
            .concat();

        let idx: Vec<u32> = clipped_meshes
            .iter()
            .map(|x| match &x.primitive {
                egui::epaint::Primitive::Mesh(mesh) => mesh.indices.clone(),
                _ => unimplemented!(),
            })
            .collect::<Vec<Vec<u32>>>()
            .concat();

        self.vertex_buffer
            .update_dynamic_vertex_buffer(0, &bgfx::Memory::copy(&vtx));

        self.index_buffer
            .update_dynamic_index_buffer(0, &bgfx::Memory::copy(&idx));
        for egui::ClippedPrimitive {
            clip_rect,
            primitive,
        } in clipped_meshes
        {
            self.paint_mesh(
                pixels_per_point,
                vertex_offset,
                index_offset,
                &clip_rect,
                primitive,
            );

            vertex_offset += match primitive {
                epaint::Primitive::Mesh(mesh) => mesh.vertices.len() as u32,
                epaint::Primitive::Callback(_pc) => 0u32,
            };
            index_offset += match primitive {
                epaint::Primitive::Mesh(mesh) => mesh.indices.len() as u32,
                epaint::Primitive::Callback(_pc) => 0u32,
            };
        }

        for &id in &texture_delta.free {
            self.free_texture(id);
        }
    }

    pub fn set_texture(&mut self, texture_id: egui::TextureId, delta: &egui::epaint::ImageDelta) {
        if let Some(texture) = self.get_texture(texture_id) {
            bgfx::set_texture(
                0,
                &self.texture_uni,
                texture,
                (SamplerFlags::UVW_CLAMP).bits(),
            );
        } else {
            let pixels: Vec<(u8, u8, u8, u8)> = match &delta.image {
                egui::ImageData::Color(image) => {
                    assert_eq!(
                        image.width() * image.height(),
                        image.pixels.len(),
                        "Mismatch between texture size and texel count"
                    );
                    image.pixels.iter().map(|color| color.to_tuple()).collect()
                }
                egui::ImageData::Font(image) => {
                    let gamma = 1.0;
                    image
                        .srgba_pixels(gamma)
                        .map(|color| color.to_tuple())
                        .collect()
                }
            };
            if let Some(pos) = delta.pos {
                // update a subregion in the texture
                if let Some(texture) = self.get_texture(texture_id) {
                    // TODO: maybe don't make a copy of the pixel data for this fn?
                    bgfx::update_texture_2d(
                        &texture,
                        0,
                        0,
                        pos[0] as _,
                        pos[1] as _,
                        delta.image.width() as _,
                        delta.image.height() as _,
                        &bgfx::Memory::copy(pixels.as_slice()),
                        0,
                    );
                }
            } else {
                let texture = bgfx::Texture::create_texture_2d(
                    delta.image.width() as _,
                    delta.image.height() as _,
                    false,
                    0,
                    bgfx::TextureFormat::RGBA8,
                    bgfx::TextureFlags::SRGB.bits(),
                    &bgfx::Memory::copy(pixels.as_slice()),
                );
                self.textures.insert(texture_id, texture);
            }
        }
    }

    pub fn get_texture(&self, texture_id: egui::TextureId) -> Option<&bgfx::Texture> {
        // the glow impl copies the texture, i dont know why
        self.textures.get(&texture_id)
    }

    pub fn free_texture(&mut self, texture_id: egui::TextureId) {
        // TODO does bgfx automatically free the texture resource when
        // there are no references to it?
        self.textures.remove(&texture_id);
    }
}

pub struct EguiBgfx {
    painter: Painter,
    egui_ctx: egui::Context,
    egui_winit: egui_winit::State,
    shapes: Vec<egui::epaint::ClippedShape>,
    textures_delta: egui::TexturesDelta,
}

impl EguiBgfx {
    pub fn new(window: &winit::window::Window) -> Self {
        let painter = Painter::new().expect("failed to create bgfx painter");
        Self {
            painter,
            egui_ctx: Default::default(),
            egui_winit: {
                let caps = bgfx::CapsLimits::new();

                let max_texture_size = caps.max_texture_size;
                println!("max texture size: {:?}", max_texture_size);
                // TODO waiting on bgfx-rs maintainer; texture size is 4096 on my machine lol
                egui_winit::State::new(4096usize, window)
            },
            shapes: Default::default(),
            textures_delta: Default::default(),
        }
    }

    pub fn run(
        &mut self,
        window: &winit::window::Window,
        run_ui: impl FnMut(&egui::Context),
    ) -> bool {
        self.egui_ctx.set_visuals(egui::Visuals {
            dark_mode: false,
            window_rounding: Rounding {
                nw: 0.0,
                ne: 0.0,
                sw: 0.0,
                se: 0.0,
            },
            window_shadow: Shadow {
                extrusion: 0.0,
                color: Color32::BLACK,
            },
            ..Default::default()
        });

        let raw_input = self.egui_winit.take_egui_input(window);
        let egui::FullOutput {
            platform_output,
            needs_repaint,
            textures_delta,
            shapes,
        } = self.egui_ctx.run(raw_input, run_ui);

        self.egui_winit
            .handle_platform_output(window, &self.egui_ctx, platform_output);

        self.shapes = shapes;
        self.textures_delta.append(textures_delta);
        needs_repaint
    }

    pub fn paint(&mut self) {
        let shapes = std::mem::take(&mut self.shapes);
        let clipped_meshes = self.egui_ctx.tessellate(shapes);
        // let textures = self.textures_delta;
        self.painter.paint_and_update_textures(
            self.egui_ctx.pixels_per_point(),
            &clipped_meshes,
            &self.textures_delta,
        )
    }

    /// Returns `true` if egui wants exclusive use of this event
    /// (e.g. a mouse click on an egui window, or entering text into a text field).
    /// For instance, if you use egui for a game, you want to first call this
    /// and only when this returns `false` pass on the events to your game.
    ///
    /// Note that egui uses `tab` to move focus between elements, so this will always return `true` for tabs.
    pub fn on_event(&mut self, event: &winit::event::WindowEvent<'_>) -> bool {
        self.egui_winit.on_event(&self.egui_ctx, event)
    }

    pub fn egui_ctx(&self) -> &egui::Context {
        &self.egui_ctx
    }
}
