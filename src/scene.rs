use crate::asset_browser::{ImageAsset, AssetBrowser};
use crate::utils;
use crate::utils::create_shader_program;
use bgfx::*;
use bgfx_rs::bgfx;
use egui::*;
use egui_extras::RetainedImage;

pub struct UiElement {
    name: Option<String>,
    texture: Texture,
    left: f32,
    right: f32,
    top: f32,
    bottom: f32,
}

pub struct Scene {
    vertex_buffer: DynamicVertexBuffer,
    index_buffer: DynamicIndexBuffer,
    default_shader: Program,
    canvas_texture: Texture,
    white_texture: Texture,
    tex_color_uniform: Uniform,
    draw_grid: bool,
    test: Vec<UiElement>,
}

#[repr(packed)]
#[allow(dead_code)]
struct PosColorTexcoordVertex {
    x: f32,
    y: f32,
    z: f32,
    abgr: u32,
    u: f32,
    v: f32,
}

impl Scene {
    pub fn new() -> Self {
        let layout_pos_color_texcoord = VertexLayoutBuilder::new();
        layout_pos_color_texcoord
            .begin(RendererType::Noop)
            .add(Attrib::Position, 3, AttribType::Float, AddArgs::default())
            .add(
                Attrib::Color0,
                4,
                AttribType::Uint8,
                AddArgs {
                    normalized: true,
                    as_int: false,
                },
            )
            .add(Attrib::TexCoord0, 2, AttribType::Float, AddArgs::default())
            .end();
        let mut quad_mesh_tex = vec![
            PosColorTexcoordVertex {
                x: -640.0,
                y: -360.0,
                z: 0.0,
                abgr: 0xFF_FF_BE_00,
                u: 0.0,
                v: 1.0,
            },
            PosColorTexcoordVertex {
                x: 640.0,
                y: -360.0,
                z: 0.0,
                abgr: 0xFF_FF_BE_00,
                u: 1.0,
                v: 1.0,
            },
            PosColorTexcoordVertex {
                x: -640.0,
                y: 360.0,
                z: 0.0,
                abgr: 0xFF_FF_BE_00,
                u: 0.0,
                v: 0.0,
            },
            PosColorTexcoordVertex {
                x: 640.0,
                y: 360.0,
                z: 0.0,
                abgr: 0xFF_FF_BE_00,
                u: 1.0,
                v: 0.0,
            },
        ];

        let mut indices = vec![0u16, 1, 2, 2, 1, 3];

        let mut count = quad_mesh_tex.len() as u16;
        let step = 25;
        let left = -640.0 - step as f32;
        for xx in (left as i32..=640).step_by(step) {
            quad_mesh_tex.push(PosColorTexcoordVertex {
                x: xx as f32,
                y: -360.0,
                z: 0.0,
                abgr: 0xFF_FF_FF_FF,
                u: 0.0,
                v: 0.0,
            });

            quad_mesh_tex.push(PosColorTexcoordVertex {
                x: xx as f32,
                y: 360.0,
                z: 0.0,
                abgr: 0xFF_FF_FF_FF,
                u: 0.0,
                v: 0.0,
            });
            indices.push(count);
            indices.push(count + 1);
            count += 2;
        }
        for yy in (-360..=360).step_by(step) {
            quad_mesh_tex.push(PosColorTexcoordVertex {
                x: -640.0,
                y: yy as f32,
                z: 0.0,
                abgr: 0xFF_FF_FF_00,
                u: 0.0,
                v: 0.0,
            });

            quad_mesh_tex.push(PosColorTexcoordVertex {
                x: 640.0,
                y: yy as f32,
                z: 0.0,
                abgr: 0xFF_FF_FF_00,
                u: 0.0,
                v: 0.0,
            });
            indices.push(count);
            indices.push(count + 1);
            count += 2;
        }

        let vbuf = bgfx::create_dynamic_vertex_buffer_mem(
            &bgfx::Memory::copy(&quad_mesh_tex),
            &layout_pos_color_texcoord,
            bgfx::BufferFlags::ALLOW_RESIZE.bits(),
        );
        let white_texture = bgfx::create_texture_2d(
            1,
            1,
            false,
            0,
            TextureFormat::RGBA8,
            TextureFlags::SRGB.bits(),
            &bgfx::Memory::copy(&vec![0xFFu8, 0xFF, 0xFF, 0xFF]),
        );
        Self {
            vertex_buffer: vbuf,
            index_buffer: {
                bgfx::create_dynamic_index_buffer_mem(
                    &bgfx::Memory::copy(&indices),
                    bgfx::BufferFlags::ALLOW_RESIZE.bits(),
                )
            },
            default_shader: create_shader_program("vs_quad_textured", "ps_quad_textured")
                .expect("shaders failed to load"),
            canvas_texture: {
                let path = "C:/src/rust/equinox/sponge_converted.png";
                let img = image::io::Reader::open(path)
                    .expect("failed to load image")
                    .decode()
                    .expect("error decoding img")
                    .to_rgba8();
                Texture::create_texture_2d(
                    img.width() as u16,
                    img.height() as u16,
                    false,
                    0,
                    TextureFormat::RGBA8,
                    TextureFlags::SRGB.bits(),
                    &bgfx::Memory::copy(&img.as_flat_samples().as_slice()),
                )
            },
            tex_color_uniform: Uniform::create("s_texColor", UniformType::Sampler, 1),
            white_texture,
            draw_grid: false,
            test: Default::default(),
        }
    }

    pub fn render(&self) {
        bgfx::set_dynamic_vertex_buffer(0, &self.vertex_buffer, 0, 5);
        bgfx::set_dynamic_index_buffer(&self.index_buffer, 0, 6);
        bgfx::set_texture(
            0,
            &self.tex_color_uniform,
            &self.canvas_texture,
            std::u32::MAX,
        );
        bgfx::submit(0, &self.default_shader, SubmitArgs::default());

        if self.draw_grid {
            bgfx::set_dynamic_vertex_buffer(0, &self.vertex_buffer, 4, std::u32::MAX);
            bgfx::set_dynamic_index_buffer(&self.index_buffer, 6, std::u32::MAX);
            let state = (StateWriteFlags::R
                | StateWriteFlags::G
                | StateWriteFlags::B
                | StateWriteFlags::A
                | StateWriteFlags::Z)
                .bits()
                | StateCullFlags::CW.bits()
                | utils::bgfx_state_blend_func(
                    StateBlendFlags::ONE.bits(),           // srcColor blend
                    StateBlendFlags::INV_SRC_ALPHA.bits(), // dstColor blend
                    StateBlendFlags::INV_DST_ALPHA.bits(), // alphaSrc blend
                    StateBlendFlags::ONE.bits(),           // dstColor blend
                )
                | StatePtFlags::LINES.bits();
            bgfx::set_texture(
                0,
                &self.tex_color_uniform,
                &self.white_texture,
                std::u32::MAX,
            );
            bgfx::set_state(state, std::u32::MAX);
            bgfx::submit(0, &self.default_shader, Default::default());
        }
    }

    pub fn add_elements(&mut self, asset_pool: &AssetBrowser) {
        let images_to_add = asset_pool.take_added_imgs();
        for (dir, img_index) in images_to_add {
            if let Some(img) = asset_pool.get_dir_assets(dir) {
                self.add_image(&img[*img_index]);
            }
        }
    }

    pub fn add_image(&mut self, asset: &ImageAsset) {
        let texture = utils::texture_from_path(asset.path.as_path()).expect("failed to create texture in Scene::add_image");
        let img = UiElement {
            name: Some(asset.path.to_str().expect("how do you have a invalid path at this point?").to_owned()),
            texture,
            left: 0f32,
            right: 100f32,
            top: 0f32,
            bottom: 100f32,
        };
        println!("added {:?}", asset.path);
    }

    pub fn update_ui(&mut self, ui: &mut egui::Ui) {
        ui.group(|ui| {
            ui.label("scene controls");
            ui.separator();
            ui.horizontal(|ui| {
                ui.checkbox(&mut self.draw_grid, "grid");
            });
        });
    }
}
