use bgfx_rs::*;
use egui::ColorImage;
use egui_extras::RetainedImage;
use image::ImageError;
use std::collections::HashMap;
use std::path::{Path, PathBuf};
use std::sync::mpsc;
use wfd;

use crate::utils;

#[derive(Debug, PartialEq, Clone, Copy)]
pub enum ThumbnailSize {
    Small = 32,
    Medium = 64,
    Large = 128,
}

impl Default for ThumbnailSize {
    fn default() -> Self {
        ThumbnailSize::Small
    }
}

// data required for an image asset
pub struct ImageAsset {
    pub path: PathBuf,           // the whole path, for finding the img on disk
    pub image: image::RgbaImage, // encapsulates the bytes and size
    pub thumbnail: egui_extras::RetainedImage, // thumbnail image used by the asset picker UI. This is a shrunk version of the original texture.
}

pub struct AssetBrowser {
    asset_map: HashMap<PathBuf, Vec<ImageAsset>>, // mapping of directory -> Vec of (image_name, pixels)
    images_to_add: Vec<(PathBuf, usize)>, // when we click on an image, store its coordinates for the scene to pick up and load later in the frame
    search_term: HashMap<PathBuf, String>,
    thumbnail_size: ThumbnailSize,
    directory_image_tx: mpsc::Sender<(PathBuf, ImageAsset)>,
    image_rx: mpsc::Receiver<(PathBuf, ImageAsset)>,
}

impl AssetBrowser {
    pub fn new() -> AssetBrowser {
        let (tx, rx) = mpsc::channel();

        AssetBrowser {
            asset_map: Default::default(),
            images_to_add: Default::default(),
            search_term: Default::default(),
            thumbnail_size: Default::default(),
            directory_image_tx: tx,
            image_rx: rx,
        }
    }

    pub fn take_added_imgs(&self) -> &Vec<(PathBuf, usize)> {
        &self.images_to_add
    }

    pub fn draw(self: &mut Self, ctx: &egui::Context) {
        // async requirement, file watchers, etc, will only grow.
        egui::Window::new("Asset Picker")
            .resizable(true)
            .scroll2([true, true])
            .min_width(0.0)
            .min_height(0.0)
            .show(&ctx, |ui| {
                ui.horizontal(|ui| {
                    egui::ComboBox::from_label("preview size")
                        .selected_text(format!("{:?}", &self.thumbnail_size))
                        .show_ui(ui, |ui| {
                            ui.selectable_value(
                                &mut self.thumbnail_size,
                                ThumbnailSize::Small,
                                "Small",
                            );
                            ui.selectable_value(
                                &mut self.thumbnail_size,
                                ThumbnailSize::Medium,
                                "Medium",
                            );
                            ui.selectable_value(
                                &mut self.thumbnail_size,
                                ThumbnailSize::Large,
                                "Large",
                            );
                        });
                });
                if ui.button("add folder").clicked() {
                    let tx = self.directory_image_tx.to_owned();
                    let thumbnail_size = self.thumbnail_size;
                    std::thread::spawn(move || {
                        match wfd::open_dialog(wfd::DialogParams {
                            title: "Open asset folder",
                            options: wfd::FOS_PICKFOLDERS,
                            ..Default::default()
                        }) {
                            // TODO async or do the file loading in a different thread
                            Ok(result) => {
                                let selected_dir = result.selected_file_path;
                                if selected_dir.is_dir() {
                                    let images = selected_dir
                                        .read_dir()
                                        .expect("failed to read user selected dir")
                                        .filter_map(|entry| match entry {
                                            Ok(entry) => {
                                                if AssetBrowser::is_supported_asset(&entry.path()) {
                                                    Some(entry.path())
                                                } else {
                                                    None
                                                }
                                            }
                                            Err(_) => None,
                                        });
                                    for f in images {
                                        tx.send((
                                            selected_dir.to_owned(),
                                            AssetBrowser::load_image_asset(
                                                f.as_path(),
                                                thumbnail_size,
                                            )
                                            .expect(""),
                                        ))
                                        .unwrap();
                                    }
                                }
                            }
                            Err(error) => println!("{:?}", error),
                        };
                    });
                }

                let window_rect = ui.available_rect_before_wrap();
                // TODO if the current directory has already been added, dont insert the image
                // add the images that have been prepared by the loader thread as they become
                // available
                if let Ok(recvd) = self.image_rx.try_recv() {
                    let (dir, img) = recvd;
                    self.asset_map.entry(dir).or_default().push(img);
                }

                for (dir, listing) in self.asset_map.iter() {
                    ui.collapsing(AssetBrowser::pretty_path(dir), |ui| {
                        ui.text_edit_singleline(
                            self.search_term.entry(dir.to_path_buf()).or_default(),
                        );
                        egui::ScrollArea::vertical().show(ui, |ui| {
                            egui::Grid::new(dir.parent().unwrap().to_string_lossy())
                                .striped(true)
                                .show(ui, |ui| {
                                    let mut image_index = 0;
                                    for image in listing {
                                        let button_hover_response = ui
                                            .group(|ui| {
                                                ui.vertical_centered_justified(|ui| {
                                                    image.thumbnail.show(ui);
                                                    ui.button(AssetBrowser::pretty_path(image.path.as_path()))
                                                });
                                            })
                                            .response;
                                        let button_rect = button_hover_response.rect;
                                        if button_rect.right()
                                            + (2.0 * self.thumbnail_size as u32 as f32)
                                            > window_rect.right()
                                        {
                                            ui.end_row();
                                        }
                                        if button_hover_response
                                            .on_hover_cursor(egui::CursorIcon::PointingHand)
                                            .on_hover_ui(|ui| {
                                                ui.label(format!(
                                                    "{:?} {}",
                                                    image.thumbnail.size_vec2(),
                                                    AssetBrowser::pretty_path(&PathBuf::from(
                                                        image.path.to_str().unwrap()
                                                    ),)
                                                ));
                                            })
                                            .clicked()
                                        {
                                            // push the coords for the clicked image
                                            println!("{}", image_index);
                                            self.images_to_add.push((dir.to_owned(), image_index));
                                        }
                                        image_index += 1;
                                    }
                                });
                        })
                    });
                }
                ui.allocate_space(ui.available_size());
            });
    }

    pub fn load_as_thumbnail(
        path: &Path,
        thumbnail_size: ThumbnailSize,
    ) -> Result<RetainedImage, ImageError> {
        let img = image::io::Reader::open(path)?.decode()?;
        //let img = img.resize_to_fill(512, 512, image::imageops::Triangle);
        let img = img.thumbnail(thumbnail_size as u32, thumbnail_size as u32);
        let size = [img.width() as _, img.height() as _];
        Ok(RetainedImage::from_color_image(
            path.to_str().unwrap(),
            egui::ColorImage::from_rgba_unmultiplied(
                size,
                img.to_rgba8().as_flat_samples().as_slice(),
            ),
        ))
    }

    // get the ImageAssets corresponding to a directory
    pub fn get_dir_assets(&self, path: &PathBuf) -> Option<&Vec<ImageAsset>> {
        self.asset_map.get(path)
    }

    pub fn clear_pending_images(&mut self) {
        self.images_to_add.clear();
    }

    /// helper fns
    pub fn load_image_asset(
        path: &Path,
        thumbnail_size: ThumbnailSize,
    ) -> Result<ImageAsset, image::ImageError> {
        let img = image::io::Reader::open(path)?.decode()?;
        let thumbnail = img.thumbnail(thumbnail_size as u32, thumbnail_size as u32);

        let thumbnail = ColorImage::from_rgba_unmultiplied(
            [thumbnail.width() as _, thumbnail.height() as _],
            thumbnail.to_rgba8().as_flat_samples().as_slice(),
        );
        let thumbnail = RetainedImage::from_color_image(AssetBrowser::pretty_path(path), thumbnail);
        let img = img.to_rgba8();
        Ok(ImageAsset {
            path: path.to_path_buf(),
            thumbnail,
            image: img,
        })
    }

    pub fn load_color_image(
        path: &Path,
        thumbnail_size: ThumbnailSize,
    ) -> Result<egui::ColorImage, ImageError> {
        let img = image::io::Reader::open(path)?.decode()?;
        //let img = img.resize_to_fill(512, 512, image::imageops::Triangle);
        let img = img.thumbnail(thumbnail_size as u32, thumbnail_size as u32);
        let size = [img.width() as _, img.height() as _];
        Ok(egui::ColorImage::from_rgba_unmultiplied(
            size,
            img.to_rgba8().as_flat_samples().as_slice(),
        ))
    }

    pub fn is_supported_asset(asset_path: &Path) -> bool {
        asset_path.is_file()
            && match asset_path.extension().unwrap_or_default().to_str().unwrap() {
                "png" | "tiff" | "jpg" => true,
                _ => false,
            }
    }

    // used for taking long paths and shortening them for the ui
    pub fn pretty_path(path: &Path) -> String {
        if path.is_dir() {
            path.strip_prefix(path.parent().unwrap())
                .unwrap()
                .to_str()
                .unwrap_or(path.to_str().unwrap())
                .to_owned()
        } else {
            path.file_name()
                .unwrap()
                .to_str()
                .unwrap_or_default()
                .to_string()
        }
    }

    //
    // resize all the loaded thumbnails
    //
    pub fn resize_thumbnails() {}
}
