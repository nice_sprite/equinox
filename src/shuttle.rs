/// event bus for communicating between systems 
///

use std::collections:: HashMap;
use std::path::PathBuf;

trait Listener<T> {
    fn on_event(event: &T);
}
struct EventBus {

    asset_clicked_listeners: Vec::<dyn Fn((PathBuf, usize))->()>,

}
