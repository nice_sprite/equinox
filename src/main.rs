use bgfx::*;
use bgfx_rs::bgfx;
pub mod scene;
use egui::{Color32, Vec2};
use glam::{Mat4, Vec3};
use scene::Scene;
pub mod asset_browser;
use asset_browser::AssetBrowser;
pub mod camera;
use camera::Camera;
pub mod egui_bgfx;
pub mod utils;
use egui_bgfx::EguiBgfx;
use egui_extras::RetainedImage;
use raw_window_handle::{HasRawWindowHandle, RawWindowHandle};
use utils::{color_as_u32_be, create_shader_program};
use winit::{
    dpi::{PhysicalPosition, PhysicalSize},
    event::{Event, WindowEvent},
    event_loop::{ControlFlow, EventLoop},
    window::Window,
    window::WindowBuilder,
};

pub mod shuttle;

use crate::utils::load_image_from_path;

fn get_platform_data(window: &Window) -> PlatformData {
    let mut pd = PlatformData::new();

    match window.raw_window_handle() {
        #[cfg(any(
            target_os = "linux",
            target_os = "dragonfly",
            target_os = "freebsd",
            target_os = "netbsd",
            target_os = "openbsd"
        ))]
        RawWindowHandle::Xlib(data) => {
            pd.nwh = data.window as *mut _;
            pd.ndt = data.display as *mut _;
        }
        #[cfg(any(
            target_os = "linux",
            target_os = "dragonfly",
            target_os = "freebsd",
            target_os = "netbsd",
            target_os = "openbsd"
        ))]
        RawWindowHandle::Wayland(data) => {
            pd.ndt = data.surface; // same as window, on wayland there ins't a concept of windows
            pd.nwh = data.display;
        }

        #[cfg(target_os = "macos")]
        RawWindowHandle::MacOS(data) => {
            pd.nwh = data.ns_window;
        }

        #[cfg(target_os = "windows")]
        RawWindowHandle::Win32(data) => {
            pd.nwh = data.hwnd;
        }
        _ => panic!("Unsupported Window Manager"),
    }

    return pd;
}

#[repr(packed)]
#[allow(dead_code)]
struct PosColorVertex {
    x: f32,
    y: f32,
    z: f32,
    abgr: u32,
}

#[repr(packed)]
#[allow(dead_code)]
struct PosColorTexcoordVertex {
    x: f32,
    y: f32,
    z: f32,
    abgr: u32,
    u: f32,
    v: f32,
}

#[rustfmt::skip]
#[allow(dead_code)]
static QUAD_MESH: [PosColorVertex; 4] = [
    PosColorVertex { x: -640.0, y: -360.0, z: 1.0, abgr: 0xFF_FF_BE_00},
    PosColorVertex { x: 640.0, y: -360.0, z: 1.0,  abgr: 0xFF_FF_BE_00},
    PosColorVertex { x: -640.0, y: 360.0, z: 1.0,  abgr: 0xFF_FF_BE_00},
    PosColorVertex { x: 640.0, y: 360.0, z: 1.0,   abgr: 0xFF_FF_BE_00},
];

#[rustfmt::skip]
static QUAD_MESH_TEX: [PosColorTexcoordVertex; 4] = [
    PosColorTexcoordVertex { x: -640.0, y: -360.0, z: 0.0, abgr: 0xFF_FF_BE_00, u: 0.0, v: 1.0},
    PosColorTexcoordVertex { x: 640.0, y: -360.0, z: 0.0,  abgr: 0xFF_FF_BE_00, u: 1.0, v: 1.0},
    PosColorTexcoordVertex { x: -640.0, y: 360.0, z: 0.0,  abgr: 0xFF_FF_BE_00, u: 0.0, v: 0.0},
    PosColorTexcoordVertex { x: 640.0, y: 360.0, z: 0.0,   abgr: 0xFF_FF_BE_00, u: 1.0, v: 0.0},
];

static INDEX_LIST: [u16; 6] = [0, 1, 2, 2, 1, 3];

struct Equinox {
    bgfx_init: Init,
    egui_bgfx: EguiBgfx,
    state_flags: u64,
    window: Window,
    mouse_pos: PhysicalPosition<f64>,
    window_size: PhysicalSize<u32>,
    clear_color: [f32; 4],
    asset_browser: AssetBrowser,
    camera: Camera,
    scene: Scene,
}

impl Equinox {
    pub fn new(event_loop: &EventLoop<()>) -> Self {
        let img_bytes = image::io::Reader::open("c:/src/rust/equinox/runtime/assets/icon4.png")
            .expect("failed to load icon")
            .decode()
            .expect("error decoding icon")
            .to_rgba8();
        let icon = winit::window::Icon::from_rgba(
            img_bytes.as_flat_samples().as_slice().to_vec(),
            img_bytes.width() as u32,
            img_bytes.height() as u32,
        )
        .expect("could not create icon");
        let window = WindowBuilder::new()
            .with_title("Equinox")
            .with_inner_size(winit::dpi::LogicalSize::new(1600.0f32, 900.0f32))
            .with_window_icon(Some(icon))
            .build(&event_loop)
            .unwrap();

        let mut init = Init::new();
        init.type_r = RendererType::Direct3D11;
        init.resolution.width = 1280;
        init.resolution.height = 720;
        init.resolution.reset = (ResetFlags::VSYNC | ResetFlags::SRGB_BACKBUFFER).bits();
        init.platform_data = get_platform_data(&window);
        if !bgfx::init(&init) {
            panic!("failed to init bgfx");
        }
        bgfx::set_debug((DebugFlags::TEXT).bits());
        bgfx::set_view_clear(
            0,
            ClearFlags::COLOR.bits() | ClearFlags::DEPTH.bits(),
            SetViewClearArgs {
                rgba: 0x000000FF,
                ..Default::default()
            },
        );
        bgfx::set_view_mode(0, ViewMode::Sequential);
        let egui_bgfx = EguiBgfx::new(&window);

        Equinox {
            bgfx_init: init,
            egui_bgfx,
            window,
            state_flags: (StateWriteFlags::R
                | StateWriteFlags::G
                | StateWriteFlags::B
                | StateWriteFlags::A
                | StateWriteFlags::Z)
                .bits()
                | StateDepthTestFlags::LESS.bits()
                | StateCullFlags::CW.bits()
                | utils::bgfx_state_blend_func(
                    StateBlendFlags::ONE.bits(),           // srcColor blend
                    StateBlendFlags::INV_SRC_ALPHA.bits(), // dstColor blend
                    StateBlendFlags::INV_DST_ALPHA.bits(), // alphaSrc blend
                    StateBlendFlags::ONE.bits(),           // dstColor blend
                ),
            mouse_pos: Default::default(),
            window_size: Default::default(),
            clear_color: Default::default(),
            asset_browser: AssetBrowser::new(),
            camera: Camera::new(),
            scene: Scene::new(),
        }
    }

    pub fn on_event(&mut self, event: &WindowEvent<'_>) {
        self.camera.handle_input(event);
        match event {
            WindowEvent::CursorMoved { position, .. } => {
                self.mouse_pos = *position;
            }
            _ => (),
        }
    }

    pub fn draw_ui(&mut self) {
        self.egui_bgfx.run(&self.window, |ctx| {
            egui::TopBottomPanel::top("Asset Picker").show(&ctx, |ui| {
                ui.horizontal(|ui| {
                    ui.button("file"); // TODO open a file menu for save, open, import, etc
                    ui.button("view"); // TODO open drop down with different windows
                });
            });

            egui::SidePanel::left("editor")
                .resizable(false)
                .show(&ctx, |ui| {
                    ui.label(format!(
                        "mouse: ({}, {})",
                        self.mouse_pos.x, self.mouse_pos.y
                    ));

                    ui.horizontal(|ui| {
                        ui.label("background color:");
                        ui.color_edit_button_rgba_unmultiplied(&mut self.clear_color);
                    });

                    self.camera.update_ui(ui);
                    self.scene.update_ui(ui);
                });

            egui::SidePanel::right("right side")
                .resizable(false)
                .show(&ctx, |ui| {
                    ui.label(format!("screen: {:?}", self.window.inner_size()));
                });
            self.asset_browser.draw(&ctx);
            self.scene.add_elements(&self.asset_browser);
        });
        self.egui_bgfx.paint();
    }
}

fn main() -> () {
    let event_loop = EventLoop::new();
    let mut app = Equinox::new(&event_loop);

    {
        event_loop.run(move |event, _, control_flow| {
            *control_flow = ControlFlow::Poll;
            match event {
                Event::WindowEvent {
                    event: WindowEvent::CloseRequested,
                    ..
                } => {
                    println!("window is closing");
                    *control_flow = ControlFlow::Exit;
                }
                Event::WindowEvent { event, .. } => {
                    if !app.egui_bgfx.on_event(&event) {
                        app.on_event(&event);
                    }
                    ()
                }
                Event::MainEventsCleared => {
                    app.window.request_redraw();
                    let window_size = app.window.inner_size();
                    if app.window_size != window_size {
                        bgfx::reset(
                            window_size.width,
                            window_size.height,
                            ResetArgs {
                                flags: (ResetFlags::SRGB_BACKBUFFER | ResetFlags::VSYNC).bits(),
                                format: app.bgfx_init.resolution.format,
                            },
                        );
                        app.window_size = window_size;
                    }

                    bgfx::set_view_clear(
                        0,
                        (ClearFlags::COLOR | ClearFlags::DEPTH).bits(),
                        SetViewClearArgs {
                            rgba: color_as_u32_be(&app.clear_color.map(|x| (255.0 * x) as u8)),
                            ..Default::default()
                        },
                    );
                    let aspect = window_size.width as f32 / window_size.height as f32;
                    let persp = Mat4::perspective_lh(
                        80.0 * std::f32::consts::PI / 180.0,
                        aspect,
                        0.1,
                        5000.0,
                    );
                    bgfx::touch(0);
                    bgfx::set_view_rect(0, 0, 0, window_size.width as _, window_size.height as _);
                    bgfx::set_state(app.state_flags, 0);
                    bgfx::set_view_transform(
                        0,
                        &app.camera.calc_view_mat().to_cols_array(),
                        &persp.to_cols_array(),
                    );
                    app.scene.render();

                    app.draw_ui();
                    bgfx::frame(false);
                    app.asset_browser.clear_pending_images(); // we added the images by this point and dont need to remember them across frames
                }
                Event::LoopDestroyed => {
                    println!("shutting down");
                    // bgfx::shutdown();
                }
                _ => (),
            }
        });
    }

    // bgfx::shutdown();
    // Ok(())
}
