/*
    This file contains functions for general purpose tasks like loading shaders & images
*/
use bgfx::*;
use bgfx_rs::bgfx;
use std::{path::PathBuf, u8};

pub fn load_shader_file(name: &str) -> std::io::Result<Vec<u8>> {
    let mut path = PathBuf::with_capacity(512);
    path.push("c:/src/rust/equinox/runtime/shaders");
    match bgfx::get_renderer_type() {
        RendererType::Direct3D11 => path.push("dx11"),
        RendererType::OpenGL => path.push("glsl"),
        RendererType::Metal => path.push("metal"),
        RendererType::OpenGLES => path.push("essl"),
        RendererType::Vulkan => path.push("spirv"),
        e => panic!("Unsupported render type {:#?}", e),
    }
    path.push(format!("{}.bin", name));
    let mut data = std::fs::read(path)?;
    data.push(0);
    Ok(data)
}

pub fn create_shader_program(vs: &str, ps: &str) -> std::io::Result<Program> {
    let vs_data = load_shader_file(vs)?;
    let ps_data = load_shader_file(ps)?;
    let vs_data = bgfx::Memory::copy(&vs_data);
    let ps_data = bgfx::Memory::copy(&ps_data);
    let vs_shader = bgfx::create_shader(&vs_data);
    let ps_shader = bgfx::create_shader(&ps_data);
    Ok(bgfx::create_program(&vs_shader, &ps_shader, false))
}

pub fn bgfx_state_blend_func(
    src_color: u64,
    dst_color: u64,
    src_alpha: u64,
    dst_alpha: u64,
) -> u64 {
    0u64 | (src_color | (dst_color << 4)) | ((src_alpha | (dst_alpha << 4)) << 8)
}

pub fn color_as_u32_be(array: &[u8; 4]) -> u32 {
    ((array[0] as u32) << 24)
        + ((array[1] as u32) << 16)
        + ((array[2] as u32) << 8)
        + ((array[3] as u32) << 0)
}

pub fn color_as_u32_le(array: &[u8; 4]) -> u32 {
    ((array[0] as u32) << 0)
        + ((array[1] as u32) << 8)
        + ((array[2] as u32) << 16)
        + ((array[3] as u32) << 24)
}

// loads a egui::ColorImage from path
pub fn load_image_from_path(path: &std::path::Path) -> Result<egui::ColorImage, image::ImageError> {
    let img = image::io::Reader::open(path)?.decode()?;
    let size = [img.width() as _, img.height() as _];
    Ok(egui::ColorImage::from_rgba_unmultiplied(
        size,
        img.to_rgba8().as_flat_samples().as_slice(),
    ))
}

// loads a bgfx::texture from path
pub fn texture_from_path(path: &std::path::Path) -> Result<bgfx::Texture, image::ImageError> {
    let img = image::io::Reader::open(path)?.decode()?;
    Ok(bgfx::create_texture_2d(
        img.width() as u16,
        img.height() as u16,
        false,
        0,
        bgfx::TextureFormat::RGBA8,
        bgfx::TextureFlags::NONE.bits(),
        &bgfx::Memory::copy(&img.to_rgba8()),
    ))
}

// returns the raw pixels as RGBA8
pub fn load_image(path: &std::path::Path) -> Result<([u32; 2], Vec<u8>), image::ImageError> {
    let img = image::io::Reader::open(path)?.decode()?;
    Ok(([img.width(), img.height()], img.to_rgba8().to_vec()))
}
