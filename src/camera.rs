use glam::{Mat4, Vec3};
use winit::event::ElementState;
use winit::event::MouseButton;
use winit::window::CursorIcon;
use winit::{dpi::PhysicalPosition, event::WindowEvent};
pub struct Camera {
    panning: bool,
    zooming: bool,
    last_mouse_pos: PhysicalPosition<f64>,
    at: Vec3,
    eye: Vec3,
    up: Vec3,
    z_near: f32,
    z_far: f32,
    zoom_sensitivity: f32,
}

// TODO adjust the sensitivity vars so that there is a roughly linear speed gradient
// because currently, zooming in closely and then panning results in super fast pannign
impl Camera {
    pub fn new() -> Self {
        let at = Vec3::new(0.0, 0.0, 0.0);
        let eye = Vec3::new(0.0, 0.0, -920.0);
        let up = Vec3::new(0.0, 1.0, 0.0);
        Camera {
            panning: false,
            zooming: false,
            last_mouse_pos: Default::default(),
            at,
            eye,
            up,
            zoom_sensitivity: 20.0,
            z_far: -5_000.0,
            z_near: -0.01,
        }
    }

    pub fn handle_input(&mut self, input_event: &WindowEvent<'_>) {
        match input_event {
            WindowEvent::MouseInput { state, button, .. } => {
                self.panning = match state {
                    ElementState::Pressed => true,
                    ElementState::Released => false,
                } && match button {
                    MouseButton::Left => true,
                    _ => false,
                };
            }
            WindowEvent::CursorMoved { position, .. } => {
                if self.panning {
                    self.translate(
                        (position.x - self.last_mouse_pos.x) as f32,
                        (position.y - self.last_mouse_pos.y) as f32,
                    );
                }
                self.last_mouse_pos = *position;
            }
            WindowEvent::MouseWheel { delta, .. } => match delta {
                winit::event::MouseScrollDelta::PixelDelta(scroll) => {
                    self.eye.z += self.zoom_sensitivity * (scroll.y as f32);
                    self.eye.z = self.eye.z.clamp(self.z_far + 5.0, self.z_near - 1.0);
                }
                winit::event::MouseScrollDelta::LineDelta(_, y) => {
                    self.eye.z += self.zoom_sensitivity * y;
                    self.eye.z = self.eye.z.clamp(self.z_far + 5.0, self.z_near - 1.0);
                }
            },
            _ => (),
        }
    }

    pub fn calc_view_mat(&self) -> Mat4 {
        Mat4::look_at_lh(self.eye, self.at, self.up)
    }

    pub fn update_ui(&mut self, ui: &mut egui::Ui) {
        ui.group(|ui| {
            ui.label("camera controls");
            ui.separator();
            ui.horizontal(|ui| {
                ui.label("zoom");
                ui.add(egui::Slider::new(&mut self.eye.z, -5000.0..=0.01));
            });
            if ui.button("reset view").clicked() {
                self.at = Vec3::new(0.0, 0.0, 0.0);
                self.eye = Vec3::new(0.0, 0.0, -920.0);
                self.up = Vec3::new(0.0, 1.0, 0.0);
            }
        });
    }

    fn translate(&mut self, dx: f32, dy: f32) {
        self.at.x += -dx;
        self.at.y += dy;
        self.eye.x += -dx;
        self.eye.y += dy;
    }
}
