# Equinox
Equinox generates Lua code for UI layout, animations, events, and shaders. 
Written in rust 
## Project goals: 
- 144hz 1440p live preview of all animations and interactions
- simulate gameplay events
- allow users to iterate on UI design fast
- completely cut out the need to relaunch the game just to tweak gameplay numbers
- code-free: generate all necessary assets and code inplace
- live update from game over local loopback socket 
- provide documentation/hints about game systems and how to use them for the cases that are impossible to cover automatically
- shader hot-reload for custom UI materials
- match or exceed bo3 quadcache budget performance
## Progress gifs
### custom egui backend fully integrated and basic scene/camera controls
- The ui library being rendered by my own custom backend 
![custom egui backend fully integrated and basic scene/camera controls](/dev_progress/rendering_demo.gif)

### multithreaded asset loading
- Delay is artificially added to show the rendering thread is not blocked by the asset loading thread. If there was no artificial delay it would load too fast to see.
![multithreaded asset loading](/dev_progress/multithread_asset_loading.gif)

### Soon: canvas interactions, mouse picking, and event system :)


