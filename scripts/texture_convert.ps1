$texturec = "C:\src\bgfx\.build\win64_vs2017\bin\texturecDebug.exe" # path to shader compiler
$converted_textured = "../runtime/assets/converted_textures"
New-Item -ItemType "directory" $converted_textured -Force

$staging_dir = "../assets/"
Get-ChildItem $staging_dir -Filter "*.jpg" |
ForEach-Object {
    $texture_out = $converted_textured + "/" + $_.BaseName + ".png"
    Write-Output $texture_out
    Start-Process $texturec -NoNewWindow -ArgumentList "-f", $_.FullName, "-o",  $texture_out
}

Get-ChildItem $staging_dir -Filter "*.tiff" |
ForEach-Object {
    $texture_out = $converted_textured + "/" + $_.BaseName + ".png"
    Write-Output $texture_out
    Start-Process $texturec -NoNewWindow -ArgumentList "-f", $_.FullName, "-o",  $texture_out
}

Get-ChildItem $staging_dir -Filter "*.tga" |
ForEach-Object {
    $texture_out = $converted_textured + "/" + $_.BaseName + ".png"
    Write-Output $texture_out
    Start-Process $texturec -NoNewWindow -ArgumentList "-f", $_.FullName, "-o",  $texture_out
}

Get-ChildItem $staging_dir -Filter "*.dds" |
ForEach-Object {
    $texture_out = $converted_textured + "/" + $_.BaseName + ".png"
    Write-Output $texture_out
    Start-Process $texturec -NoNewWindow -ArgumentList "-f", $_.FullName, "-o",  $texture_out
}

Get-ChildItem $staging_dir -Filter "*.png" |
ForEach-Object {
    $texture_out = $converted_textured + "/" + $_.BaseName + ".png"
    Write-Output $texture_out
    Start-Process $texturec -NoNewWindow -ArgumentList "-f", $_.FullName, "-o",  $texture_out
}