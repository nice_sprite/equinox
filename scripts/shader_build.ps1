$shaderc = "C:\src\bgfx\.build\win64_vs2017\bin\shadercDebug.exe" # path to shader compiler
$shader_src = "../shader_src" 
$platforms = @{"dx11" = "s_5_0"; "spirv" = "spirv"; "glsl" = "440" }
$shader_bin = "../runtime/shaders/" # TODO directX only, not sure if cross platform matters
$bgfx_shader_inc = "C:\src\rust\shader_src\bgfx_shader.sh"

foreach ( $platform in $platforms.Keys ) {
    New-Item -ItemType "directory" ($shader_bin + $platform) -Force
}

# use ps_ and vs_ prefixes for shaders

# do fragment shaders
foreach ( $platform in $platforms.Keys) {
    Get-ChildItem $shader_src -Filter ps_* | 
    ForEach-Object {
        if ($platforms[$platform] -eq "s_5_0") {
            $shader_model = "ps_5_0"
        }
        else {
            $shader_model = $platforms[$platform]
        }
        Write-Output $_.BaseName
        $shader_out = ($shader_bin + $platform + "/" + $_.BaseName + ".bin")
        Start-Process $shaderc -ArgumentList "--profile", $shader_model, "-f", $_.FullName, "-o", $shader_out, "--type fragment", "--platform windows", "-i", $bgfx_shader_inc -NoNewWindow
    }

    # do fragment shaders
    Get-ChildItem $shader_src -Filter vs_* | 
    ForEach-Object {
        if ($platforms[$platform] -eq "s_5_0") {
            $shader_model = "vs_5_0"
        }
        else {
            $shader_model = $platforms[$platform]
        }
        Write-Output $_.BaseName
        $shader_out = ($shader_bin + $platform + "/" + $_.BaseName + ".bin")
        Start-Process $shaderc -ArgumentList "--profile", $shader_model, "-f", $_.FullName, "-o", $shader_out, "--type vertex", "--platform windows", "-i", $bgfx_shader_inc -NoNewWindow
    }
}
