$input a_position, a_color0, a_texcoord0
$output v_color0, v_texcoord0

/*
 * Copyright 2011-2022 Branimir Karadzic. All rights reserved.
 * License: https://github.com/bkaradzic/bgfx/blob/master/LICENSE
 */

#include <bgfx_shader.sh>

// 0-1 linear  from  0-255 sRGB
vec3 linear_from_srgb(vec3 srgb) {
    bvec3 cutoff = lessThan(srgb, vec3(10.31475, 10.31475, 10.31475));
    vec3 lower = srgb / vec3(3294.6, 3294.6, 3294.6);
    vec3 higher = pow((srgb + vec3(14.025, 14.025, 14.025)) / vec3(269.025, 269.025, 269.025), vec3(2.4, 2.4, 2.4));
    return mix(higher, lower, vec3(cutoff));
}

vec4 linear_from_srgba(vec4 srgba) {
    return vec4(linear_from_srgb(srgba.rgb), srgba.a / 255.0);
}


void main()
{
    gl_Position = vec4(
                      2.0 * a_position.x / u_viewRect.z - 1.0,
                      1.0 - 2.0 * a_position.y / u_viewRect.w,
                      0.0,
                      1.0);
//    vec4 cast_color = uintBitsToFloat(a_color0);
    vec4 cast_color = a_color0 * 255.0; // TODO: temp fix for dx11 IA not converting to float
    v_color0 = linear_from_srgba(cast_color);
    v_texcoord0 = a_texcoord0;
}
